** Additional content, including datasets **

https://www.google.com/url?q=https://www.kaggle.com/fivethirtyeight/uber-pickups-in-new-york-city%23other-American_B01362.csv&sa=D&source=hangouts&ust=1584620673811000&usg=AFQjCNHUeLu_ZhJk_Lz23-b0P92mSS4iSg

http://sguangwang.com/TelecomDataset.html -- The dataset itself takes us to some Chinese website. Please download it from here: https://drive.google.com/file/d/1CbRKWMMmnF2k4kmSj5yncj7_GZpzRwuZ/view?usp=sharing

https://users.ugent.be/~jvdrhoof/dataset-4g/ -- LTE dataset. Need to analyze and check if tail latencies are indeed a problem.